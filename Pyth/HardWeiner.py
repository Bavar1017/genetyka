# Wojciechowski Michał
# Hardy–Weinberg principle calculations
import math

def chi(dom, het, rec):
    pop = dom + rec + het
    p = (2 * dom + het) / (2*pop)
    q = (2 * rec + het) / (2*pop)
    edom = p ** 2 * pop
    ehet = 2 * p * q *pop
    erec = q ** 2 * pop
    if dom == 0 and rec == 0:
        chi = "nope"
    elif dom == 0 and het == 0:
        chi = "yes"
    elif het == 0 and rec == 0:
        chi = "yes"
    else:
        chi = (((dom-edom) ** 2)/edom) + (((het-ehet) ** 2)/ehet) + (((rec - erec) ** 2)/erec)
    if chi < 0.0001:
        chi = 0
    return chi

# Find genotypes freq. given allele freq.
def hard_wiener(d: float, f: float):
    aa = f ** 2
    Aa = 2 * d * f
    AA = d ** 2
    return {"aa": str(aa), "Aa": str(Aa), "AA": str(AA), "AA+Aa": str(Aa+AA)}

# Find allele freq. genotype freq. and phenotype freq. given numbers of phenotypes
def rev_hard_wiener(a1a1, a1a2, a2a2):
    A1 = (2 * a1a1 + a1a2) / (2 * (a1a1 + a1a2 + a2a2))
    A2 = (2 * a2a2 + a1a2) / (2 * (a1a1 + a1a2 + a2a2))
    freqA1A1 = a1a1 / (a1a1 + a1a2 + a2a2)
    freqA1A2 = a1a2 / (a1a1 + a1a2 + a2a2)
    freqA2A2 = a2a2 / (a1a1 + a1a2 + a2a2)
    freqPhenDom = freqA1A1 + freqA1A2
    freqPhenRec = freqA2A2
    if A1+A2 < 1.05 and A1+A2 > 0.95 and hard_wiener(A1,A2)["aa"] == str(freqA2A2) and hard_wiener(A1,A2)["Aa"] == str(freqA1A2) and hard_wiener(A1,A2)["AA"]== str(freqA1A1):
        balance = "Perfectly balanced, as all things should be. Little one, it's simple calculus."
    else:
        balance = "nope"
    return {"freqA1": A1, "freqA2": A2, "freqA1A1": freqA1A1, "freqA1A2": freqA1A2, "freqA2A2": freqA2A2,
            "freqPhenDom": freqPhenDom, "freqPhenRec": freqPhenRec,"balance":balance}
# Find allele freq. given homozygous freq.
def find_hom(p):
    p = math.sqrt(p)
    return p
i = 1
# loop for chosing what to do
while i == 1:
    x = input("Hard, Find or Rev: ")
    if x.lower() == "hard":
        p, q = input("input p and q:").split()
        print(hard_wiener(float(p), float(q)))
    elif x.lower() == "rev":
        a, b, c = input("please input A1A1, A1A2, and A2A2: ").split()
        print(rev_hard_wiener(float(a), float(b), float(c)))
    elif x.lower() == "find":
        a = float(input("insest hom. freq.: "))
        print(find_hom(a))
    elif x.lower() == "chi":
        a,b,c = input("input: AA, Aa, aa: ").split()
        print(chi(float(a),float(b),float(c)))
    elif x.lower() == "halt": # stopping program
        i += 2
    else:
        print("Wrong, try again")
