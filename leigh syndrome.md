# Zespół leigha
podostra martwicza encefalo(mielo)patia/Leigh syndrome/Leigh disease/SNEM
## objawy
Mogą pojawić się nagle lub powoli, zazwyczaj w wyniku sytuacji większego zapotrzebowania organizmu na ATP.  
Zazwyczaj pojawiają się pomiędzy 3 i 12 miesiącem życia, ale mogą wystąpić znacznie później.
Choroba może zacząć powodować szkody jeszcze przed narodzeniem.  
### niektóre z objawów to:
#### zburzenia układu pokarmowego
- zaburzenia czynności mięśni układu pokarmowego
- zaparcia
- zgaga
- wymioty
- anoreksja
- dysfagia
- problemy z prawidłowym wzrostem

#### oddechowe
- nieprawidłowe oddychanie
- hipoksja

#### sercowe
- kardiomiopatia przerostowa
- ubytek przegrody międzykomorowej

#### nerwowe
- drgawki
- upośledzenie umysłowe
- hipotonia
- dystonia
- neuropatia
- hiporefleksja
- ataksja
- pląsawica
- spastyczność
- drżenie kończyn

#### skórne
- nienaturalny zapach skóry
- przebarwienia skóry
- hipertrichoza

#### metaboliczne/nerkowe
- kwasica mleczanowa
- tubulopatie
- zespół nerczycowy
- zespół Fanconiego

#### endokrynne
- cukrzyca
- zaburzenia tarczycy

#### słuchowe
- utrata słuchu
- neuropatia słuchowa

#### zaburzenia immunologiczne
#### wzrokowe
- oczopląs
- oftalmopareza
- retinopatia barwnikowa
- problemy z podążaniem wzrokiem

#### mięśniowo-szkieletowe
- wiotkość mięśni
- ptoza
- stopa wydrążona

## przyczyny
SNEM może być spowodowany mutacjami w ponad 75 różnych genów znajdujących się zarówno w mtDNA jak i nDNA. mutacja w którymkolwiek genie kodującym cytochrom c, koenzym Q10 lub jeden z 5 kompleksów łańcucha oddechowego może doprowadzić do SNEM  
W większości przypadków SNEM jest spowodowany mutacjami w nDNA, a w 20% spowodowany jest mutacjami w mtDNA  
większość genów związanych z SNEM jest zaangażowane w proces wytwarzania energii w mitochondriach, wiele mutacji odpowiedzialnych za SNEM uszkadza lub uniemożliwia powstawanie elementów łańcucha oddechowego biorącego udziału w syntezie ATP.  
Najczęstszymi przyczynami są nieprawidłowości związane z kompleksem 1 za który odpowiada 25 genów, z kompleksem 4 (jest to przyczyną 15% przypadków), a najczęstszą mutacją mtDNA jest mutacje genu MT-ATP6, która jest przyczyną 10% przypadków


### najczęstsze geny których mutacje odpowiedzialne są za SNEM

| Gen      | czego dotyczy                                    | lokalizacja |
|----------|--------------------------------------------------|-------------|
| BCSIL    | kompleks 3                                       | nDNA        |
| C12ORF65 | kompleks 1                                       | nDNA        |
| COX10    | kompleks 4                                       | nDNA        |
| COX11    | kompleks 4                                       | nDNA        |
| COX15    | kompleks 4                                       | nDNA        |
| FOXRED1  | kompleks 1                                       | nDNA        |
| GFM1     | translacja mitochondrialna                       | nDNA        |
| LRPPRC   | kompleks 4                                       | nDNA        |
| MT-ATP6  | kompleks 5                                       | mtDNA       |
| MT-ND1   | kompleks 1                                       | mtDNA       |
| MT-ND2   | kompleks 1                                       | mtDNA       |
| MT-ND3   | kompleks 1                                       | mtDNA       |
| MT-ND4   | kompleks 1                                       | mtDNA       |
| MT-ND5   | kompleks 1                                       | mtDNA       |
| MT-ND6   | kompleks 1                                       | mtDNA       |
| MT-TK    | kompleks 1                                       | mtDNA       |
| MT-TLI   | kompleks 1                                       | mtDNA       |
| MT-TV    | kompleks 1                                       | mtDNA       |
| MT-TW    | kompleks 1                                       | mtDNA       |
| NDUFA1   | kompleks 1                                       | chromosom x |
| NDUFA2   | kompleks 1                                       | nDNA        |
| NDUFA4   | kompleks 4                                       | nDNA        |
| NDUFA9   | kompleks 1                                       | nDNA        |
| NDUFA10  | kompleks 1                                       | nDNA        |
| NDUFA11  | kompleks 1                                       | nDNA        |
| NDUFA12  | kompleks 1                                       | nDNA        |
| NDUFAF2  | kompleks 1                                       | nDNA        |
| NDUFAF3  | kompleks 1                                       | nDNA        |
| NDUFAF6  | kompleks 1                                       | nDNA        |
| NDUFS1   | kompleks 1                                       | nDNA        |
| NDUFS2   | kompleks 1                                       | nDNA        |
| NDUFS3   | kompleks 1                                       | nDNA        |
| NDUFS4   | kompleks 1                                       | nDNA        |
| NDUFS7   | kompleks 1                                       | nDNA        |
| NDUFS8   | kompleks 1                                       | nDNA        |
| NDUFV1   | kompleks 1                                       | nDNA        |
| PDHA1    | kompleksu dehydrogenazy pirogronianowej          | chromosom x |
| PDHB     | kompleksu dehydrogenazy pirogronianowej          | chromosom x |
| PDHX     | kompleksu dehydrogenazy pirogronianowej          | chromosom x |
| PDSS2    | ubichinon (koenzym Q10)                          | nDNA        |
| PET100   | kompleks 4                                       | nDNA        |
| SCO2     | kompleks 4                                       | nDNA        |
| SDHA     | kompleks 2                                       | nDNA        |
| SDHAF1   | kompleks 2                                       | nDNA        |
| SLC19A3  | problem z transportem wit. B1                    | nDNA        |
| SUCLA2   | utrata DNA mitochondrialnego                     | nDNA        |
| SUCLG1   | utrata DNA mitochondrialnego                     | nDNA        |
| SURF1    | kompleks 4                                       | nDNA        |
| TACO1    | kompleks 4                                       | nDNA        |
| TTC19    | kompleks 3                                       | nDNA        |
| UQCRQ    | kompleks 3                                       | nDNA        |

### niedobór PDH
może być spowadowany przez mutację jednego z wielu genów odpowiedzialnych za powstawanie kompleksu PDH, u osób z niedoborem kompleksu PDH często występuje zespół Leigha.  
mogą wystąpić nieprawidłowości w każdej z trzech podjednostek kompleksu enzymatycznego, może to prowadzić do zmniejszonej aktywności PDH i w rezultacie mniejszej ilości pirogronianu.  
#### ciekawostka
pomimo większej śmiertelności u mężczyzn, kobiety z niedoborem PDH umierają młodziej
### niedobór kompleksów łańcucha oddechowego
mutacje powodują nieprawidłowe działanie kompleksów łańcucha oddechowego.  
w przypadku niedoborów poszczególnych kompleksów łańcucha oddechowego, u niektórych stwierdza się zespół Leigha natomiast u innych nie, przyczyna tego zjawiska nie jest do końca znana.
### niedobór oksydazy cytochromu C
najczęstszą mutacją odpowiadającą za niedobór COX jest mutacja genu SURF1 , powoduje brak białka SURF1, co upośledza syntezę ATP przez mitochondria.  
Szczególnym przypadkiem jest Lac-St-Jean cytochrome c oxidase deficiency, różni się tym od niedoboru związanego z SURF1, że dotyczy on syntezy peptydów odpowiedzialnych za stabilność mRNA mitochondrialnego. Nie wiadomo dlaczego regulacja stabilności mRNA wpływa na niedobór  COX.
<br/>
<br/>
<br/>
### mutacja genu ATP6
mutacja tego genu prowadzi do najczęstszego przypadku zespołu Leigha dziedziczonego w linii matczynej. Zmienia ona funkcje kanału białkowego komlepleksu 5, co prowadzi do utraty zdolności do syntezy ATP przez ten kompleks. kiedy chory ma ponad 90% mitochondriów uznaje się, że mają oni zespół Leigha dziedziczony w lini matczynej (MILS), jeśli mieści się w 50%-60% jest to zespół NARP.
## diagnoza

ze względu na różnorodność objawów i brak specyficznego biochemicznego lub molekularnego defektu diagnoza bazowana jest najczęściej na podstawie charakterystycznego obrazu zmian patologicznych mózgu widocznych na MRI lub w trakcie autopsji.

Neuroobrazowanie może jednak nie dawać widocznych zmian ze względu na konkretne mutacje lub występujące zmiany mogą nie być objawem SNEM a innych chorób takich jak np.: niedobór PDH czy niedobór kompleksu 5 bez SNEM lub inne niezwiązane z mitochondriami choroby np. zatrucie CO, zespół Huntingtona.

Z tego względu diagnozę zespołu Leigha bazuje się na kombinacji obrazowania MRI, nieprawidłowości biochemicznych np. podwyższone stężenie mleczanów w płynie mózgowo-rdzeniowym, oraz obrazu klinicznego zgodnego z chorobą.
## leczenie

Obecnie nie ma żadnego konkretnego sposobu leczenia zespołu Leigha i specyficzne terapie zaburzeń mitochondrialnych nie są ogólnie dostępne.  
Pomimo coraz dokładniejszego poznania mechanizmów tej choroby opcje jej leczenia są niezwykle ograniczone, zazwyczaj leczy się ją objawowo.

## dziedziczność

- w linii matczynej wraz z mtDNA
- wraz z chromosomem X jako gen recesywny
- jako gen recesywny na chromosomie autosomalnym

## częstość
globalnie - co najmniej 1 na 40000 noworodków  
zależy od regionu np:
- Saguenay–Lac-Saint-Jean, region w kanadyjskiej prowincji Quebec  
1 na 2000 noworodków
- wyspy owcze  
1 na 1700 noworodków  
(kolejny powód dla którego nie warto się tam przeprowadzać)

##### źródła
Leigh syndrome - Genetics Home Reference - NIH [Internet]. U.S. National Library of Medicine. National Institutes of Health; [cited 2020Apr11]. Available from: https://ghr.nlm.nih.gov/condition/leigh-syndrome
Saneto  R, Ruhoy I. The genetics of Leigh syndrome and its implications for clinical practice and risk management. The Application of Clinical Genetics. 2014Nov17;:221–34.
